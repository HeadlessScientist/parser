import yaml
from dataclasses import dataclass


@dataclass
class Static:
    headers: dict
    url: str


def read_static(file: str):
    with open(f'static/{file}.yaml', 'r') as f:
        static = yaml.safe_load(f)

    return Static(
        headers = static["headers"],
        url = static["url"],
    )


static = read_static("static")