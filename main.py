import json
from requests import get, Response

from static.static import static
from parsing.parser import get_employees


if __name__=="__main__":
    res: Response = get(static.url, headers=static.headers)
    print(res.status_code)
    with open(f'result/res.txt', "w") as f:
        f.writelines([f"{quote}\n" for quote in get_employees(res.text)])
        # f.writelines([f"{str(quote.get('field', 'None'))}:{quote.text.encode().decode('utf-8')}\n" for quote in get_employees(res.text)])
        # f.writelines(get_employees(res.text))
