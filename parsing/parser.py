import re
from bs4 import BeautifulSoup
from dataclasses import dataclass
from typing import Optional, List


@dataclass
class Employ:
    email: str
    job_title: str
    # city: Optional[str]
    # name: str


def get_job_title(employ) -> str:
    parent = employ.parent.text.replace(employ.text, '')
    return re.split(r'\d', parent)[0]


def get_employees(data: str) -> List[Employ]:
    res = []
    soup = BeautifulSoup(data, 'lxml')
    quotes = soup.find_all('div', class_=re.compile('tn-atom')) #tn-atom t396__elem tn-elem tn-elem__
    emails = soup.find_all('a', href=re.compile('mailto:'))
    for employ in emails:
        employ_ = Employ(
            email=employ.text,
            job_title=get_job_title(employ),
            # name=employ.parent.text,
            # city=employ.parent.parent.previous_sibling,
        )
        if not employ_.job_title == "" and employ_.job_title.find("Telegram") == -1:
            res.append(employ_)
    return res